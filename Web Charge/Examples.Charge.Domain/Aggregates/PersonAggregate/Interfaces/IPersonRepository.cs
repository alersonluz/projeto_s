﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Charge.Domain.Aggregates.PersonAggregate.Interfaces
{
    public interface IPersonRepository
    {
        Task<IEnumerable<PersonAggregate.Person>> FindAllAsync();
        Task<PersonAggregate.Person> FindAsync(int id);
        Task<PersonAggregate.Person> Remove(int id);
        Task<PersonAggregate.Person> Add(Person person);
        Task<PersonAggregate.Person> Update(Person person);
    }
}
