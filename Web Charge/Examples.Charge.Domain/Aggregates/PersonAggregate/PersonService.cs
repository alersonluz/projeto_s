﻿using Examples.Charge.Domain.Aggregates.PersonAggregate.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examples.Charge.Domain.Aggregates.PersonAggregate
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IPersonPhoneRepository _personPhoneRepository;
        public PersonService(IPersonRepository personRepository, IPersonPhoneRepository personPhoneRepository)
        {
            _personRepository = personRepository;
            _personPhoneRepository = personPhoneRepository;

        }

        public async Task<Person> Add(Person person)
        {
            return await _personRepository.Add(person);
        }

        public async Task<List<Person>> FindAllAsync() => (await _personRepository.FindAllAsync()).ToList();

        public async Task<Person> FindAsync(int id)
        {
            return await _personRepository.FindAsync(id);
        }

        public async Task<Person> Remove(int id)
        {
            return await _personRepository.Remove(id);
        }

        public async Task<Person> Update(Person person)
        {
            await _personPhoneRepository.RemoveByPersonId(person.BusinessEntityID);
            var resp = await _personRepository.Update(person);
            return resp;
        }
    }
}
