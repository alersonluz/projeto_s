﻿using AutoMapper;
using Examples.Charge.Application.Dtos;
using Examples.Charge.Application.Interfaces;
using Examples.Charge.Application.Messages.Response;
using Examples.Charge.Domain.Aggregates.PersonAggregate;
using Examples.Charge.Domain.Aggregates.PersonAggregate.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examples.Charge.Application.Facade
{
    public class PersonFacade : IPersonFacade
    {
        private readonly IPersonService _personService;
        private readonly IMapper _mapper;

        public PersonFacade(IPersonService personService, IMapper mapper)
        {
            _personService = personService;
            _mapper = mapper;
        }

        public async Task<PersonResponse> Add(PersonDto personDto)
        {
            var model = _mapper.Map<Person>(personDto);
            var result = await _personService.Add(model);
            var respose = new PersonResponse();
            respose.PersonObject = _mapper.Map<PersonDto>(result);
            return respose;
        }

        public async Task<PersonListResponse> FindAllAsync()
        {
            var result = await _personService.FindAllAsync();
            var response = new PersonListResponse();
            response.PersonObjects = new List<PersonDto>();
            response.PersonObjects.AddRange(result.Select(x => _mapper.Map<PersonDto>(x)));
            return response;
        }

        public async Task<PersonResponse> FindAsync(int id)
        {
            var result = await _personService.FindAsync(id);
            var response = new PersonResponse();
            response.PersonObject = _mapper.Map<PersonDto>(result);
            return response;
        }

        public async Task<PersonResponse> Remove(int id)
        {
            var result = await _personService.Remove(id);
            var response = new PersonResponse();
            response.PersonObject = _mapper.Map<PersonDto>(result);
            return response;
        }

        public async Task<PersonResponse> Update(PersonDto personDto)
        {
            var model = _mapper.Map<Person>(personDto);
            var result = await _personService.Update(model);
            var response = new PersonResponse();
            response.PersonObject = _mapper.Map<PersonDto>(result);
            return response;
        }
    }
}
