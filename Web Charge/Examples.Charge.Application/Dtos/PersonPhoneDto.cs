﻿namespace Examples.Charge.Application.Dtos
{
    public class PersonPhoneDto
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneNumberTypeId { get; set; }
        public PersonDto Person { get; set; }
        public PhoneNumberTypeDto PhoneNumberType { get; set; }
    }
}