﻿using AutoMapper;
using Examples.Charge.Application.Dtos;
using Examples.Charge.Application.Interfaces;
using Examples.Charge.Application.Messages.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examples.Charge.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : BaseController
    {
        private IPersonFacade _facade;

        public PersonController(IPersonFacade facade, IMapper mapper)
        {
            _facade = facade;
        }

        [HttpGet]
        public async Task<ActionResult<PersonListResponse>> Get()
        {
            return Response(await _facade.FindAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PersonResponse>> Get(int id)
        {
            return Response(await _facade.FindAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PersonDto person)
        {
            var resp = await _facade.Add(person);
            return Response(resp.PersonObject.Id, resp.PersonObject);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PersonDto person)
        {
            var resp = await _facade.Update(person);
            return Response(resp);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var resp = await _facade.Remove(id);
            return Response(resp);
        }
    }
}
