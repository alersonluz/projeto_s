﻿using Examples.Charge.Domain.Aggregates.PersonAggregate;
using Examples.Charge.Domain.Aggregates.PersonAggregate.Interfaces;
using Examples.Charge.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Examples.Charge.Infra.Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly ExampleContext _context;

        public PersonRepository(ExampleContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task<IEnumerable<Person>> FindAllAsync() => await Task.Run(() => _context.Person);

        public async Task<Person> Add(Person person)
        {
            var task = await _context.Person.AddAsync(person);
            await _context.SaveChangesAsync();
            return task.Entity;
        }


        public async Task<Person> FindAsync(int id)
        {
            var model = await _context.Person.FindAsync(id);
            _context.Entry(model).Collection("Phones").Load();
            foreach (var p in model.Phones)
                _context.Entry(p).Reference("PhoneNumberType").Load();
            return model;
        }

        public async Task<Person> Remove(int id)
        {
            var entry = _context.Person.Remove(await FindAsync(id));
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<Person> Update(Person person)
        {
            var model = _context.Person.Update(person);
            await _context.SaveChangesAsync();
            return model.Entity;
        }
    }
}
