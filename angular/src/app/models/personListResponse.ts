import { BaseResponse } from "./baseResponse";
import { Person } from "./person";

export class PersonListResponse extends BaseResponse {

    personObjects: Person[];

    constructor(obj?) {
        super(obj)
        if (obj) {
            this.personObjects = obj.exampleObjects.map(x => new Person(x));
        }
    }
}