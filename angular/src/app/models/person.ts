import { Phone } from "./phone";

export class Person {

    id: number;
    name: string;
    phones: Phone[];

    constructor(obj?) {
        if (obj) {
            this.id = obj.id;
            this.name = obj.name;
            this.phones = obj.phones.map(x => new Phone(x));
        }
    }
}