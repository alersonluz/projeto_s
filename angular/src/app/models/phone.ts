import { PhoneNumberType } from "./phoneNumberType";

export class Phone {

    businessEntityID: number;
    phoneNumber: string;
    phoneNumberTypeID: number;
    phoneNumberType: PhoneNumberType;

    constructor(obj?) {
        if (obj) {
            this.businessEntityID = obj.businessEntityID;
            this.phoneNumber = obj.phoneNumber;
            this.phoneNumberTypeID = obj.phoneNumberTypeID;
            this.phoneNumberType = new PhoneNumberType(obj.phoneNumberType);
        }
    }
}