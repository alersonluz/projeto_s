export class BaseResponse {

    success: boolean;
    erros: any[];

    constructor(obj?) {
        this.success = true;
        this.erros = null;

        if (obj) {
            this.success = obj.success;
            this.erros = obj.erros;
        }
    }
}