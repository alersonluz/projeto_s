import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { urlApi } from 'src/environments/environment';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeopleServiceService {

  protected port = '51103';
  protected controllerUrl = 'http://localhost:'+ this.port  + '/api/';

  constructor(
        private router: Router,
        private http: HttpClient,
  ) { 

  }

  get(url, options?): any {
    return this.http.get(
      this.controllerUrl + url, options
    );
  }

  delgetByIdete(url, id) {
    return this.http.get(
      this.controllerUrl + url + '/' + id
    ).pipe(share());
  }

  post(url, data) {
    return this.http.post(
      this.controllerUrl + url, data
    ).pipe(share());
  }

  put(url, data) {
    return this.http.put(
      this.controllerUrl + url, data
    ).pipe(share());
  }

  delete(url, id) {
    return this.http.delete(
      this.controllerUrl + url + '/' + id
    ).pipe(share());
  }
}
